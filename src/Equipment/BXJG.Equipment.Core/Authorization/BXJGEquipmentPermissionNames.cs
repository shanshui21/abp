﻿namespace BXJG.Equipment.Authorization
{
    public static class BXJGEquipmentPermissionNames
    {
        public const string BXJGEquipment = "BXJGEquipment";
        //{codegenerator}
        #region 设备信息
        public const string BXJGEquipmentEquipmentInfo = "BXJGEquipmentEquipmentInfo";
        public const string BXJGEquipmentEquipmentInfoCreate = "BXJGEquipmentEquipmentInfoCreate";
        public const string BXJGEquipmentEquipmentInfoUpdate = "BXJGEquipmentEquipmentInfoUpdate";
        public const string BXJGEquipmentEquipmentInfoDelete = "BXJGEquipmentEquipmentInfoDelete";
        #endregion
    }
}
