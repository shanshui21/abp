﻿using Abp.Domain.Repositories;
using BXJG.GeneralTree;
using BXJG.Shop.Catalogue;
using BXJG.Shop.Customer;
using BXJG.Shop.Sale;
using BXJG.WeChat.Payment;
using System;
using System.Collections.Generic;
using System.Text;

using ZLJ.Authorization.Roles;
using ZLJ.Authorization.Users;
using ZLJ.BaseInfo.Administrative;
using ZLJ.MultiTenancy;

namespace ZLJ.Shop
{
    /// <summary>
    /// 
    /// </summary>
    public class BXJGShopCustomerOrderAppService : BXJGShopCustomerOrderAppService<
            Tenant,
            User,
            Role,
            TenantManager,
            UserManager,
            OrderManager,
            CustomerManager>
    {
        public BXJGShopCustomerOrderAppService(
            IRepository<CustomerEntity<User>, long> customerRepository,
            CustomerManager customerManager,
            BXJGShopCustomerSession<User> customerSession,
            IRepository<OrderEntity<User>, long> repository,
            OrderManager orderManager,
            IRepository<AdministrativeEntity, long> generalTreeManager, 
            IRepository<ItemEntity, long> itemRepository, WeChatPaymentService weChatPaymentService)
            : base(customerRepository, customerManager, customerSession, repository, orderManager, generalTreeManager, itemRepository, weChatPaymentService)
        {
            //sd
        }
    }
}
